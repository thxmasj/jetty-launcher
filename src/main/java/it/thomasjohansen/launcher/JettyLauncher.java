package it.thomasjohansen.launcher;

import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;

import javax.servlet.ServletException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.console;

/**
 * Launch web applications (WAR files) with Jetty.
 */
public class JettyLauncher {

    private static final String privateKeyStoreResource = "/tls.jks";
    private Path baseDir;
    private List<ConnectorDescriptor> connectorDescriptors = new ArrayList<>();
    private List<ApplicationDescriptor> applicationDescriptors = new ArrayList<>();
    private boolean enableManager;
    private String managerContextPath = "/manager";

    public static void main(String[] args) throws Exception {
        if (args.length > 0)
            configureFromArguments(args);
        else
            configureDefault();
    }

    private static void configureDefault() throws Exception {
        configure()
                .addConnector(8080)
                .addApplication(
                        "/",
                        JettyLauncher.class.getProtectionDomain().getCodeSource().getLocation().getFile()
                )
                .enableManager()
                .apply()
                .launch();
    }

    private static void configureFromArguments(String[] args) throws Exception {
        Configurer configurer = configure();
        List<String> arguments = Arrays.asList(args);
        for (String argument : arguments) {
            if (argument.matches("\\d+")) {
                configurer.addConnector(Integer.parseInt(argument));
            } else if (argument.matches("/.*=.*\\.war")) {
                configurer.addApplication(argument.split("=")[0], argument.split("=")[1]);
            } else {
                configurer.addApplication(
                        argument,
                        JettyLauncher.class.getProtectionDomain().getCodeSource().getLocation().getFile()
                );
            }
        }
        configurer.enableManager().apply().launch();
    }

    private JettyLauncher() {
    }

    public static Configurer configure() {
        return new Configurer(new JettyLauncher());
    }

    public void launch() throws Exception {
        final Server server = new Server();
        //server.setBaseDir(baseDir.toAbsolutePath().toString());
        for (ConnectorDescriptor connectorDescriptor : connectorDescriptors) {
            if (connectorDescriptor.getKeyStorePath() != null)
                addSecureConnector(server, connectorDescriptor.getPort(), connectorDescriptor.getKeyStorePath());
            else
                addConnector(server, connectorDescriptor.getPort());
        }
        for (ApplicationDescriptor applicationDescriptor : applicationDescriptors) {
            addWebApplication(
                    server,
                    baseDir,
                    applicationDescriptor.getContextPath(),
                    applicationDescriptor.getLocation()
            );
        }
        // Start all webapps in parallell
        //server.getHost().setStartStopThreads(applicationDescriptors.size());
        if (enableManager) {
            //addManagerServlet(server, managerContextPath);
        }
        // Enable servlet specification annotations
        Configuration.ClassList classlist = Configuration.ClassList
                .setServerDefault(server);
        classlist.addAfter(
                org.eclipse.jetty.webapp.FragmentConfiguration.class.getName(),
                org.eclipse.jetty.plus.webapp.EnvConfiguration.class.getName(),
                org.eclipse.jetty.plus.webapp.PlusConfiguration.class.getName()
        );
        classlist.addBefore(
                org.eclipse.jetty.webapp.JettyWebXmlConfiguration.class.getName(),
                org.eclipse.jetty.annotations.AnnotationConfiguration.class.getName()
        );

        //Runtime.getRuntime().addShutdownHook(new WorkFileRemover(baseDir));
        server.start();
        server.getServer().join();
    }

    private boolean isSecured() throws IOException {
        return getClass().getResourceAsStream(privateKeyStoreResource) != null;
    }

    private void addWebApplication(
            Server server,
            Path baseDir,
            String contextPath,
            String location
    ) throws ServletException, IOException, URISyntaxException {
        WebAppContext context = new WebAppContext();
        context.setContextPath(contextPath);
        context.setWar(location);
        server.setHandler(context);
        //handleRunningFromMavenWorkspace(location, context);
        // Note that class loading is extremely slow with unpackWAR=false, so start-up and first request(s) might take
        // long time (up to minutes).
        //context.setUnpackWAR(true);
        // Directory "webapps" is not used when unpackWAR is false
        //if (context.getUnpackWAR() && !Files.exists(baseDir.resolve("webapps")))
        //    Files.createDirectory(baseDir.resolve("webapps"));
        //return context;
    }

    private void addManagerServlet(Server server, String contextPath) {
        throw new UnsupportedOperationException();
    }

    private String createPrivateKeyStore(Path directory, String password) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        return createKeyStore(directory, "privateKeyStore", privateKeyStoreResource, password);
    }

    private String createKeyStore(Path directory, String fileName, String resourceName, String password) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        InputStream in = getClass().getResourceAsStream(resourceName);
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(in, password.toCharArray());
        Path file = directory.resolve(fileName);
        FileOutputStream out = new FileOutputStream(file.toFile());
        keyStore.store(out, password.toCharArray());
        return file.toAbsolutePath().toString();
    }

    private String getPrivateKeyPassword() {
        return System.getProperty("javax.net.ssl.keyStorePassword",
                console() != null
                        ? String.valueOf(console().readPassword("Private key password> "))
                        : "changeit");
    }

    private void addSecureConnector(
            Server server,
            int port,
            Path keyStorePath
    ) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        throw new UnsupportedOperationException();
    }

    private void addConnector(Server server, int port) {
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(port);
        server.addConnector(connector);
    }

    static class WorkFileRemover extends Thread {

        private Path baseDir;

        public WorkFileRemover(Path baseDir) {
            this.baseDir = baseDir;
        }

        @Override
        public void run() {
            try {
                deleteRecursive(baseDir.toFile());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        public boolean deleteRecursive(File path) throws FileNotFoundException{
            if (!path.exists()) throw new FileNotFoundException(path.getAbsolutePath());
            boolean ret = true;
            if (path.isDirectory()){
                File[] files = path.listFiles();
                if (files != null) {
                    for (File f : files) {
                        ret = ret && deleteRecursive(f);
                    }
                }
            }
            return ret && path.delete();
        }

    }

    static class ConnectorDescriptor {

        private int port;
        private Path keyStorePath;

        public ConnectorDescriptor(int port) {
            this.port = port;
        }

        public ConnectorDescriptor(int port, Path keyStorePath) {
            this.port = port;
            this.keyStorePath = keyStorePath;
        }

        public int getPort() {
            return port;
        }

        public Path getKeyStorePath() {
            return keyStorePath;
        }

    }

    static class ApplicationDescriptor {

        private String contextPath;
        private String location;

        public ApplicationDescriptor(String contextPath, String location) {
            this.contextPath = contextPath;
            this.location = location;
        }

        public String getContextPath() {
            return contextPath;
        }

        public String getLocation() {
            return location;
        }

    }

    public static class Configurer {

        private JettyLauncher instance;

        public Configurer(JettyLauncher instance) {
            this.instance = instance;
        }

        public Configurer addConnector(int port) {
            instance.connectorDescriptors.add(new ConnectorDescriptor(port));
            return this;
        }

        public Configurer addSecureConnector(int port, Path keyStorePath) {
            instance.connectorDescriptors.add(new ConnectorDescriptor(port, keyStorePath));
            return this;
        }

        public Configurer addApplication(String contextPath, String location) {
            instance.applicationDescriptors.add(new ApplicationDescriptor(
                    contextPath,
                    location
            ));
            return this;
        }

        public Configurer enableManager() {
            instance.enableManager = true;
            instance.managerContextPath = "/manager";
            return this;
        }

        public Configurer enableManager(String contextPath) {
            instance.enableManager = true;
            instance.managerContextPath = contextPath;
            return this;
        }

        public JettyLauncher apply() throws IOException {
            instance.baseDir = Files.createTempDirectory("JettyLauncher");
            return instance;
        }

    }

}
