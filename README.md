# jetty-launcher

Launch web applications with Jetty.

# Usage

```java
JettyLauncher.configure()
        .addConnector(8080)
        .addApplication(
                "/",
                "/path/to/warfile.war"
        )
        .apply()
        .launch();
```
